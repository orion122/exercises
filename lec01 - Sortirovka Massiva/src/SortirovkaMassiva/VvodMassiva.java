package SortirovkaMassiva;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

class VvodMassiva {
	BufferedReader br;
	int k;
	
	
	VvodMassiva() {
		br = new BufferedReader(new InputStreamReader(System.in));
	}
	
	
	int vvod(String stroka) throws NumberFormatException, IOException {
		System.out.print(stroka);
		return Integer.parseInt(br.readLine());
	}
	
	
	void massiv() throws NumberFormatException, IOException {
		k = vvod("Количество элементов массива = ");
		int massiv[] = new int[k];
		int tmp;
		
		for (int i=0; i<k; i++){
			massiv[i] = vvod("Элемент массива = ");	
		}
				
		for (int i=0; i < (k-1); i++) {
			for (int j=0; j < (k-1-i); j++) {
				if (massiv[j] > massiv[j+1]) {
					tmp = massiv[j];
					massiv[j] = massiv[j+1];
					massiv[j+1] = tmp;
				}
			}
		}
		
		for (int i=0; i<k; i++) {
			System.out.print(massiv[i] + " ");
		}

	}

}
