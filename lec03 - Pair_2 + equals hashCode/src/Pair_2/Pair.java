package Pair_2;

public class Pair {
	double a;
	double b;
	
	Pair(double a, double b) {
		this.a = a;
		this.b = b;
	}
	
	static void sumPair(Pair obj1, Pair obj2) {
		System.out.println("(a, b) + (c, d) = " + "(" + (obj1.a + obj1.b) + ", " + (obj2.a + obj2.b) + ")");
		System.out.println();
	}
	
	static void mult(Pair obj1, Pair obj2, double k) {
		System.out.println("k * (a, b) = " + "(" + (obj1.a * k) + ", " + (obj1.b * k) + ")");
		System.out.println("k * (c, d) = " + "(" + (obj2.a * k) + ", " + (obj2.b * k) + ")");
		System.out.println();		
	}
	
	public boolean equals(Object otherObject) {
		if(this == otherObject) return true;
		if(otherObject == null) return false;
		if(getClass() != otherObject.getClass()) return false;
		if(!(otherObject instanceof Pair)) return false;
		Pair other = (Pair) otherObject;
				return a == other.a && b == other.b;
	}
	
	public int hashCode() {
		int result = 1;
		result = 31 * result + (int) a;
		result = 31 * result + (int) b;
		return result;
	}

}
