package Pair_2;

public class Complex extends Pair{
	
	Complex(double x, double y) {
		super(x, y);
	}
	
	static void multCompl(Pair obj1, Pair obj2) {
		System.out.println("(a, b) * (с, d) = " +
	                       "(" + (obj1.a * obj2.a - obj1.b * obj2.b) + ", " +
	                             (obj1.a * obj2.b + obj2.a * obj1.b) + ")");
		System.out.println();
	}
	
	static void raznCompl(Pair obj1, Pair obj2) {
		System.out.println("(a, b) - (с, d) = " + "(" + (obj1.a - obj1.b) + ", " + (obj2.a - obj2.b) + ")");
		System.out.println();
	}

}
