package Pair_2;

public class MainPair {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Класс Pair
		Pair objP1 = new Pair(12.5, 17.5);
		Pair objP2 = new Pair(12.5, 17.5);
		
		//Сумма пары объектов
		Pair.sumPair(objP1, objP2);
		Pair.mult(objP1, objP2, 10.0);		
		
		//Класс Complex
		Complex.multCompl(objP1, objP2);
		Complex.raznCompl(objP1, objP2);
		
		//equals() AND hashcode()
		System.out.println("equals(): " + objP1.equals(objP2));
		System.out.println("hashcode(): " + objP1.hashCode());
		System.out.println("hashcode(): " + objP2.hashCode());
	}

}
