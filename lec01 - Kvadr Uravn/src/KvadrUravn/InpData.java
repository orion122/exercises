package KvadrUravn;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

class InpData {
	BufferedReader br;
	
	InpData() {
		br = new BufferedReader(new InputStreamReader(System.in));
	}
	
	double VvodDannih(String stroka) throws NumberFormatException, IOException {
		System.out.print(stroka);
		return Double.parseDouble(br.readLine());
	}

}
