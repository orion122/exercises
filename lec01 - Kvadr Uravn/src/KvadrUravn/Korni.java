package KvadrUravn;

import java.io.IOException;

class Korni {
	double a, b, c;
	
	void dataMethod() throws NumberFormatException, IOException {
		InpData data = new InpData();		
		
		a = data.VvodDannih("a = ");
		b = data.VvodDannih("b = ");
		c = data.VvodDannih("c = ");		
	}
	
	void reshenie() {
		double x1, x2;
		double D = b*b - 4*a*c;
		
		if (D > 0) {
			x1 = (-b - Math.sqrt(D)) / (2*a);
			x2 = (-b + Math.sqrt(D)) / (2*a);
			System.out.println("x1 = " + x1);
			System.out.println("x2 = " + x2);
		}
		else if (D == 0) {
			x1 = -b / (2*a);
			System.out.println("x1 = x2 = " + x1);
		}
		else {
			System.out.println("Нет корней");
		}
	}

}
