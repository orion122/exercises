package Pair_1;

public class MainPair {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Умножение на число и сложение пар
		Pair objP1 = new Pair(2, 3);
		Pair objP2 = new Pair(4, 7);
		
		System.out.println("Умножение пары на число для первого объекта:");
		objP1.mult(5);
		
		System.out.println("Умножение пары на число для второго объекта:");
		objP1.mult(3);
		
		System.out.println("Сложение пар первого объекта:");
		objP1.sumPair();
		
		System.out.println("Сложение пар второго объекта:");
		objP2.sumPair();
		
		//Действия над денежной суммой
		Money objM1 = new Money(60, 5);
		Money objM2 = new Money(20, 50);
		Money objOper = new Money(objM1, objM2);
		
		//Сумма рублей и копеек каждого объекта как десятичная дробь
		objOper.sumRubAndKop(objM1, objM2);
		
		//Сумма денег
		System.out.println("Сумма денег:");
		objOper.sumPair(objM1, objM2);
		
		//Разность денежной суммы
		System.out.println("Разность денежной суммы:");
		objOper.raznPair(objM1, objM2);
		
		//Деление денежной суммы
		System.out.println("Деление денежной суммы:");
		objOper.delenPair(objM1, objM2);
	}
	
}
