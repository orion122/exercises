package Pair_1;

public class Money extends Pair {
	static float sumRubAndKop1;
	static float sumRubAndKop2;

	Money(int rub, int kop) {
		super(rub, kop);
	}

	static void sumRubAndKop(Pair obj1, Pair obj2) {
		sumRubAndKop1 = obj1.a + ((float) obj1.b)/100;
		sumRubAndKop2 = obj2.a + ((float) obj2.b)/100;
	}

	static void sumPair() {
		float summa = sumRubAndKop1 + sumRubAndKop2;
		System.out.println("Сумма: " + ((int) summa) + " рублей " + 
	                                   ((int) (100 * (summa - (int) summa))) + " копеек");
		System.out.println();
	}
	
	static void raznPair() {
		float raznost = Math.abs(sumRubAndKop1 - sumRubAndKop2);
		System.out.println("Разность: " + ((int) raznost) + " рублей " + 
                                          ((int) ((100 * raznost - 100 * (int) raznost))) + " копеек");
		System.out.println();
	}
	
	static void delenPair() {
		System.out.println("Частное: " + sumRubAndKop1/sumRubAndKop2);
	}
	
}