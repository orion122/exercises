package Pair_1;

public class Pair {
	int a;
	int b;
	
	Pair(int a, int b) {
		this.a = a;
		this.b = b;		
	}
	
	static void mult(Pair obj1, Pair obj2, int k) {		
		System.out.println("k * (a, b) = " + "(" + (obj1.a * k) + ", " + (obj1.b * k) + ")");
		System.out.println("k * (c, d) = " + "(" + (obj2.a * k) + ", " + (obj2.b * k) + ")");
		System.out.println();
	}
	
	static void sumPair(Pair obj1, Pair obj2) {
		System.out.println("(a,b) + (c,d) = " + "(" + (obj1.a + obj1.b) + ", " + (obj2.a + obj2.b) + ")");
		System.out.println();
	}
	
}