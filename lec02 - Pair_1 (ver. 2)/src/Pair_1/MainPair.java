package Pair_1;

public class MainPair {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Pair objP1 = new Pair(2, 3);
		Pair objP2 = new Pair(4, 7);
		
		Pair.mult(objP1, objP2, 3);
		Pair.sumPair(objP1, objP2);		
		
		Money.sumRubAndKop(objP1, objP2);
		Money.sumPair();
		Money.raznPair();
		Money.delenPair();

	}
	
}
